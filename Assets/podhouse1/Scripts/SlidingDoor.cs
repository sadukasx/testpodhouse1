﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoor : MonoBehaviour {

    public GameObject trigger;
    public GameObject rightDoor;
    public GameObject leftDoor;

    Animator rightAim;
    Animator leftAim;


	// Use this for initialization
	void Start () {
        leftAim = leftDoor.GetComponent<Animator>();
        rightAim = rightDoor.GetComponent<Animator>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("Triggered");
        SlideDoors(true);
    }


    void OnTriggerExit(Collider collider)
    {
        Debug.Log("De-Triggered");
        SlideDoors(false);
    }

    void SlideDoors(bool state)
    {
        leftAim.SetBool("slide", state);
        rightAim.SetBool("slide", state);
    }
}
