﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaptureScreenshot : MonoBehaviour {

    [SerializeField]
    GameObject blink;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ScreenShot()
    {
        StartCoroutine("CaptureIt");
    }


    IEnumerator CaptureIt()
    {
        string timeStamp = System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss");
        string fileName = "ScreenShot" + timeStamp + ".png";
        string pathToSave = fileName;
        ScreenCapture.CaptureScreenshot(pathToSave);
        //yield return new WaitForSeconds(3);
        yield return new WaitForEndOfFrame();

        StartCoroutine(DelayTime());

        //Instantiate(blink, new Vector3(0f, 0f, 0f), Quaternion.identity);

        //yield return new WaitForSeconds(2);
        //blink.SetActive(true);
    }

    IEnumerator DelayTime()
    {
        //yield return new WaitForSeconds(5f);

        yield return new WaitForEndOfFrame();

        StartCoroutine(Blink());
    }

    IEnumerator Blink()
    {
        blink.SetActive(true);
        yield return new WaitForEndOfFrame();
        blink.SetActive(false);
    }
}
